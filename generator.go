// Copyright 2023 The muslheaders Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the GO-LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"go/token"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	util "modernc.org/ccgo/v3/lib"
	ccgo "modernc.org/ccgo/v4/lib"
	"modernc.org/ccorpus2"
)

const (
	archivePath          = "assets/musl.libc.org/releases/musl-1.2.4.tar.gz"
	extractedArchivePath = "musl-1.2.4"
	muslIncludes         = "include"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	arch   string
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintf(os.Stderr, msg, args...)
	os.Exit(rc)
}

func main() {
	var err error
	if arch, err = muslArch(goos, goarch); err != nil {
		fail(1, "%s\n", err)
	}

	tempDir, err := os.MkdirTemp("", "muslheaders-generate")
	if err != nil {
		fail(1, "creating temp dir: %v\n", err)
	}

	defer func() {
		switch os.Getenv("GO_GENERATE_KEEP") {
		case "":
			os.RemoveAll(tempDir)
		default:
			fmt.Println(tempDir)
		}
	}()

	f, err := ccorpus2.FS.Open(archivePath)
	if err != nil {
		fail(1, "cannot open tar file: %v\n", err)
	}

	util.MustUntar(true, tempDir, f, nil)
	srcRoot := filepath.Join(tempDir, extractedArchivePath)
	util.MustCopyFile(true, "COPYRIGHT-MUSL", filepath.Join(srcRoot, "COPYRIGHT"), nil)
	util.MustInDir(true, srcRoot, func() error {
		util.MustShell(true, "./configure")
		util.MustShell(true, "make")
		return nil
	})

	includes := filepath.Join(srcRoot, muslIncludes)
	if err := filepath.WalkDir(includes, func(srcPath string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			return nil
		}

		rel, err := filepath.Rel(includes, srcPath)
		if err != nil {
			return err
		}

		const tag = ".h"
		if !strings.HasSuffix(srcPath, tag) {
			return nil
		}

		if _, ok := blacklist[filepath.Base(srcPath)]; ok {
			return nil
		}

		fmt.Println(rel)
		dir, file := filepath.Split(rel)
		fileNoh := file[:len(file)-len(tag)]
		destDir := filepath.Join(dir, fileNoh)
		if err := os.MkdirAll(destDir, 0755); err != nil {
			return err
		}

		pkgName := strings.ReplaceAll(fileNoh, "-", "_")
		if token.IsKeyword(pkgName) {
			pkgName += "_"
		}
		destPath := filepath.Join(destDir, fmt.Sprintf("%s_%s_%s.go", fileNoh, goos, goarch))

		if pkgName == "stdio" {
			// We need some of the types defined here. In GLibc they're included in stdio.h.
			srcPath = filepath.Join(srcRoot, "src", "internal", "stdio_impl.h")
		}
		return generate(destPath, srcPath, srcRoot, pkgName)
	}); err != nil {
		fail(1, "%s", err)
	}
}

var blacklist = map[string]struct{}{
	"kd.h":        {}, //TODO(jnml) ? /musl-1.2.4/arch/generic/bits/kd.h:1:10: include file not found: <linux/kd.h>
	"soundcard.h": {}, //TODO(jnml) ? /musl-1.2.4/arch/generic/bits/soundcard.h:1:10: include file not found: <linux/soundcard.h>
	"vt.h":        {}, //TODO(jnml) ? /musl-1.2.4/arch/generic/bits/vt.h:1:10: include file not found: <linux/vt.h>
}

func generate(destPath, srcPath, root, pkgName string) error {
	t := ccgo.NewTask(
		goos, goarch,
		[]string{
			os.Args[0],
			"--package-name", pkgName,
			"-header",
			"-ignore-asm-errors",
			"-nostdinc",
			"-nostdlib",
			"-o", destPath,

			//TODO(jnml) the prefixes are just an initial example.
			// N/A "--prefix-define", "D",
			"--prefix-enumerator", "E",
			"--prefix-external", "X",
			"--prefix-field", "F",
			"--prefix-macro", "M",
			"--prefix-tagged-enum", "TE",
			"--prefix-tagged-struct", "TS",
			"--prefix-tagged-union", "TU",
			"--prefix-typename", "TN",

			// Replacing stdio.h with stdio_impl.h requires a different configuration that
			// seems to not work well when generating other Go headers. In particular, a
			// different version of features.h is needed. But then the added include path
			// must precede the one with the other features.h and this causes some
			// complications.  But it turns out the only thing we actually need from the
			// different features.h version is the definition of 'hidden'. Putting that
			// definition right here is much simpler, even when hackish/ugly.
			`-Dhidden=__attribute__((__visibility__("hidden")))`,

			"-I", filepath.Join(root, "include"),
			"-I", filepath.Join(root, "arch", arch),
			"-I", filepath.Join(root, "arch", "generic"),
			"-I", filepath.Join(root, "obj", "include"),
			srcPath,
		},
		os.Stdout, os.Stderr, nil,
	)
	return t.Main()
}

func muslArch(goos, goarch string) (string, error) {
	switch s := fmt.Sprintf("%s/%s", goos, goarch); s {
	case "linux/amd64":
		return "x86_64", nil
	case "linux/arm64":
		return "aarch64", nil
	case "linux/riscv64":
		return "riscv64", nil
	//TODO(jnml) case "linux/...":
	// 	return "mips64"
	case "linux/386":
		return "i386", nil
	//TODO(jnml) case "linux/...":
	// 	return "powerpc64"
	//TODO(jnml) case "linux/...":
	// 	return "powerpc"
	case "linux/arm":
		return "arm", nil
	//TODO(jnml) case "linux/...":
	//	return "mipsn32"
	//TODO(jnml) case "linux/...":
	// 	return "mips"
	case "linux/s390x":
		return "s390x", nil
	default:
		return "", fmt.Errorf("unsupported/unknown target %q", s)
	}
}

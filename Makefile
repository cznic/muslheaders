# Copyright 2023 The muslheaders Authors. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

.PHONY:	all clean edit editor generate test

all:

clean:
	rm -f *.text *.out *.test
	go clean

edit:
	@touch log
	@if [ -f "Session.vim" ]; then gvim -S & else gvim -p Makefile *.go & fi

editor:
	date > log-editor
	gofmt -l -s -w *.go
	go test -failfast 2>&1 | tee -a log-editor
	go build -v -o /dev/null generator.go 2>&1 | tee -a log-editor
	go build -v -o /dev/null ./... 2>&1 | tee -a log-editor
	golint 2>&1 | tee -a log-editor
	staticcheck 2>&1 | tee -a log-editor
	date >> log-editor

generate:
	@echo "No cross-generating supported, run only on the target intended to generate"
	date > log-generate
	go run generator.go 2>&1 | tee -a log-generate
	go build -v -o /dev/null ./... 2>&1 | tee -a log-generate
	date >> log-generate

test:
	date > log-test
	go test -failfast 2>&1 | tee -a log-test
	date >> log-test
